(function (Baz) {
	'use strict';

	Baz = 'default' in Baz ? Baz['default'] : Baz;

	function Qux () {
		console.log( 'Qux' )
	}

	console.log( 'Baz', Baz )
	console.log( 'Qux', Qux )

}(Baz));