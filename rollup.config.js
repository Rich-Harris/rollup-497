import * as path from 'path';

export default {
	entry: 'src/index.js',
	dest: 'dist/bundle.js',
	external: [ path.resolve( 'src/foo/bar/baz.js' ) ],
	format: 'iife'
};
